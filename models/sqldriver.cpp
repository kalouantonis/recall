/*
 * sqlreader.cpp
 *
 *  Created on: May 30, 2013
 *      Author: slacker
 */

#include "sqldriver.h"

SQLDriver::SQLDriver(const std::string& filename)
{
	// Initialize to nothing. Easier to detect errors
	database = NULL;

	Open(filename);
}

bool SQLDriver::Open(const std::string& filename)
{
	if(sqlite3_open(filename.c_str(), &database) == SQLITE_OK)
		// Opening succeeded
		return true;

	PrintError();
	return false;
}

void SQLDriver::PrintError(void)
{
	std::cout << "SQL Error: "
			<< sqlite3_errstr(sqlite3_errcode(database))
			<< std::endl;
}

std::string SQLDriver::sanitize(char* query_string)
{
	if(query_string == NULL)
		return std::string();

	return std::string(query_string);
}

SQLDriver::qval_type SQLDriver::Query(const std::string& query)
{
	sqlite3_stmt* statement;
	SQLDriver::qval_type results;

	// TODO: Sanitize query strings
	if(sqlite3_prepare_v2(database, query.c_str(), -1, &statement, 0) == SQLITE_OK)
	{
		int column_count = sqlite3_column_count(statement);
		int result = 0;

		// Stores values for each row
		std::vector<std::string> values;

		// The first practical use of a do...while(), maybe ever!
		do
		{
			result = sqlite3_step(statement);

			for(int i = 0; i < column_count; i++)
				values.push_back(
						sanitize((char*)sqlite3_column_text(statement, i))
				);

			results.push_back(values);

			// Clear vector, ready for more input
			values.clear();
		} while(result == SQLITE_ROW);

		sqlite3_finalize(statement);
	}
	else
	{
		PrintError();
	}

	return results;
}

int SQLDriver::GetRowCount(const std::string& tablename)
{
	sqlite3_stmt *statement;

	std::string query = "SELECT * FROM " + tablename;

	if(sqlite3_prepare_v2(database, query.c_str(), -1, &statement, 0) == SQLITE_OK)
	{
		int row_count;

		int result = sqlite3_step(statement);
		for(row_count = 0; result == SQLITE_ROW; row_count++)
			result = sqlite3_step(statement);

		return row_count;

	}
	else
	{
		PrintError();
		return QUERY_FAILED;
	}
}

/**
 *
 * Gets the number of columns in the given table
 *
 * @param tablename
 * @return int  returns QUERY_FAILED if query failed
 */
int SQLDriver::GetColCount(const std::string& tablename)
{
	std::string query_string = "SELECT * FROM " + tablename;

	sqlite3_stmt *statement;

	if(sqlite3_prepare_v2(database, query_string.c_str(), -1, &statement, 0)
			== SQLITE_OK)
	{
		int col_count = sqlite3_column_count(statement);

		sqlite3_finalize(statement);

		return col_count;
	}
	else
	{
		PrintError();
		return QUERY_FAILED;
	}
}

void SQLDriver::Close(void)
{
	sqlite3_close(database);
}






