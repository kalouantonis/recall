#include "sqlwriter.h"

// TODO: Find a way to NRYS...

int SQLWriter::Insert(const std::string& table_name, 
                  const map_type& data)
{
   std::string query_string = "INSERT INTO " + table_name + ' '; 

   // Store keys and values for later processing
   std::string keys;
   std::string values;

   map_type::const_iterator i;

   // Iterate until one item before end to exclude connection
   // operators 
   for(i = data.begin(); i != --data.end(); i++)
   {
        keys += i->first + ", ";
        values += i->second + ", ";
   }

    keys += i->first;
    values += i->second;

    query_string += '(' + keys + ')' + " VALUES (" + values + ')';

   std::cout << query_string << std::endl;

   // check the size of the return query, if larger than 0, then query succeded
   // TODO: check my hypothesis :)
   return Query(query_string).size() ? QUERY_SUCCESS : QUERY_FAILED;
}


int SQLWriter::Update(const std::string& tablename,
                    const map_type& data, const map_type& where)
{
    std::string query_string = "UPDATE " + tablename + " SET ";

    map_type::const_iterator i;

    for(i = data.begin(); i != --data.end(); i++)
        query_string += i->first + " = " + i->second + ", ";

    query_string += i->first + " = " + i->second + " WHERE ";

    // They have the same type, so i can use the same iterator
    for(i = where.begin(); i != --where.end(); i++)
       query_string += i->first + " = " + i->second + ", ";

    query_string += i->first + " = " + i->second;

    std::cout << query_string << std::endl;
   
    return Query(query_string).size() ? QUERY_SUCCESS : QUERY_FAILED;
}

int SQLWriter::Update(const std::string& tablename,
                      const map_type& data, const std::string& where)
{
    std::string query_string = "UPDATE " + tablename + " SET ";

    map_type::const_iterator i;
    for(i = data.begin(); i != --data.end(); i++)
        query_string += i->first + " = " + i->second + ", ";
        
    query_string += i->first + " = " + i->second + " WHERE " + where;
    
    std::cout << query_string << std::endl;

    return Query(query_string).size() ? QUERY_SUCCESS : QUERY_FAILED;
}

int SQLWriter::Delete(const std::string& table_name, 
                  const map_type& where)
{
    std::string query_string = "DELETE FROM " + table_name + " WHERE ";

    map_type::const_iterator i;
    for(i = where.begin(); i != --where.end(); i++)
        query_string += i->first + " = " + i->second + ", ";

    query_string += i->first + " = " + i->second;

    std::cout << query_string << std::endl;

    return Query(query_string).size() ? QUERY_SUCCESS : QUERY_FAILED;
}

int SQLWriter::Delete(const std::string& tablename,
                    const std::string& where)
{
    return Query("DELETE FROM " + tablename + " WHERE " + where).size()
            ? QUERY_SUCCESS : QUERY_FAILED;
}
