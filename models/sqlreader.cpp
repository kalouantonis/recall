/*
 * sqlreader.cpp
 *
 *  Created on: Jun 1, 2013
 *      Author: slacker
 */

#include "sqlreader.h"


SQLReader::qval_type SQLReader::Get(const std::string& table_name,
		int limit, int offset)
{

	std::string query_string = "SELECT * FROM " + table_name;

    check_limit_params(query_string, limit, offset);

    std::cout << query_string << std::endl;
      
	return Query(query_string);
}

void SQLReader::check_limit_params(std::string& query_string, int limit, int offset)
{
    if(limit || offset)
        query_string += " LIMIT=" + toStr(limit) + "," + toStr(offset);
}

SQLReader::qval_type SQLReader::GetWhere(const std::string& table_name,
                const map_type& search, 
                int limit, int offset)
{
    std::string query_string = "SELECT * FROM " + table_name + " WHERE ";


    // Generate the query
    map_type::const_iterator i;
    // Iterating until n-1 items, so that last item can be included,
    // but without the AND operator
    for(i = search.begin(); i != --search.end(); i++)
        query_string += i->first + " = " + i->second + " AND ";

    query_string += i->first + " = " + i->second;

    check_limit_params(query_string, limit, offset);

    std::cout << query_string << std::endl;

    return Query(query_string);
}

SQLReader::qval_type SQLReader::GetLike(const std::string& table_name,
                              const map_type& search, 
                              int limit, int offset)
{
    std::string query_string = "SELECT * FROM " + table_name + " WHERE ";


    map_type::const_iterator i;
    for(i = search.begin(); i != --search.end(); i++)
        query_string += i->first + " LIKE %" + i->second + "% AND "; 

    query_string += i->first + " LIKE %" + i->second + "%";

    check_limit_params(query_string, limit, offset);

    std::cout << query_string << std::endl;

    return Query(query_string);
}


SQLReader::qval_type SQLReader::Select(const std::string& table_name, 
                                    const std::vector<std::string>& search,
                                    int limit, int offset)
{
    std::string query_string = "SELECT ";


    std::vector<std::string>::const_iterator i;
    for(i = search.begin(); i != --search.end(); i++)
        query_string += *i + ", ";

    query_string += *i;

    query_string += " FROM " + table_name;

    check_limit_params(query_string, limit, offset);

    std::cout << query_string << std::endl;

    return Query(query_string);
}
