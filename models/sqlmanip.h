#ifndef SQLMANIP_H
#define SQLMANIP_H

#include "sqldriver.cpp"

#include <map>

class SQLManip: public SQLDriver
{
public:
   SQLManip(const std::string& filename): SQLDriver(filename) { }

   typedef std::map<std::string, std::string> map_type;

   // columns["COLNAME"] = "DATATYPE";
   int CreateTable(const std::string& tablename, const map_type& columns);

};

#endif
