/*
 * sqlreader.h
 *
 *  Created on: May 30, 2013
 *      Author: slacker
 */

#ifndef SQLDRIVER_H
#define SQLDRIVER_H


#include <sqlite3.h>
#include <cstddef>
#include <vector>
#include <iostream>
#include <sstream>

#define QUERY_SUCCESS	 0
#define QUERY_FAILED 	-1

class SQLDriver
{
public:
	SQLDriver(const std::string& filename);

	// Open database, return false if failed
	bool Open(const std::string& filename);

	typedef std::vector<std::vector<std::string> > qval_type;
	typedef size_t size_type;

	// Perform SQL query
	qval_type Query(const std::string& query);

	int GetRowCount(const std::string& tablename);
	int GetColCount(const std::string& tablename);

	// Close connection
	void Close(void);

protected:
	void PrintError(void);

    template<typename T>
    std::string toStr(const T& t)
    {
        std::ostringstream os;
        os << t;

        return os.str();
    }

private:
	sqlite3* database;

	std::string sanitize(char* query_string);
};


#endif
