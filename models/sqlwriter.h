/*
 * sqlwriter.h
 *
 *  Created on: May 31, 2013
 *      Author: slacker
 */

#ifndef SQLWRITER_H_
#define SQLWRITER_H_

#include "sqldriver.h"

#include <map>

class SQLWriter: public SQLDriver
{
public:
	SQLWriter(const std::string& filename): SQLDriver(filename) { }

	typedef std::map<std::string, std::string> map_type;

	// Maybe use macros

    // INSERT INTO table_name(key1, key2, ..) VALUES (value1, value2, ...)
	int Insert(const std::string& table_name,
			const map_type& data);


    // UPDATE tablename SET KEY1=VAL1, KEY2=VAL2, ... WHERE LOC1=VAL
	int Update(const std::string& table_name,
			const map_type& data,
			const map_type& where);

	// where = "id = 4"
	int Update(const std::string& table_name,
			const map_type& data,
			const std::string& where);

	// DELETE FROM table_name WHERE key=value
	int Delete(const std::string& table_name,
			const map_type& where);

    int Delete(const std::string& table_name,
                const std::string& where);
};


#endif /* SQLWRITER_H_ */
