#ifndef WXSQL_H
#define WXSQL_H

#include "sqldriver.h"
#include "sqlreader.h"
#include "sqlwriter.h"

#include <wx/string.h>


class WXSQL: SQLDriver
{
public:
    WXSQL(const std::string& filename, const std::string& tablename): SQLDriver(filaname) 
    { this->tablename = tablename; }

    int CreateTable(void);

    void InsertRecord(const wxString& title, const wxString& desc = wxEmptyString,
                   const wxDateTime& data = wxDateTime(),
                   bool done = false);

    int UpdateRecord(int id, const wxString& title, const wxString& desc = wxEmptyString,
                    const wxDateTime& data = wxDateTime(),
                    bool done = false);

    int DeleteRecord(int id);

    SQLDriver::qval_type GetAll(void);

    int GetWhere(const wxString& query_string);
    int GetDataFromId(int id);

private:
    std::string tablename;

    SQLReader* reader;
    SQLWriter* writer;
};

#endif
