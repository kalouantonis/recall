/*
 * sqlreader.h
 *
 *  Created on: May 31, 2013
 *      Author: slacker
 */

#ifndef SQLREADER_H_
#define SQLREADER_H_

#include "sqldriver.h"
#include <map>
#include <iostream>

class SQLReader: public SQLDriver
{
public:
	SQLReader(const std::string& filename): SQLDriver(filename) { }

	// TODO: Use macros so that i don't need to request the tablename in every
	// query

	// I want to make this work like CodeIgniter

	// SELECT * FROM tablename LIMIT=?, ?
	qval_type Get(const std::string& table_name, int limit = 0, int offset = 0);


	typedef std::map<std::string, std::string> map_type;

	// SELECT * FROM tablename WHERE T=?
	qval_type GetWhere(const std::string& table_name,
			const map_type& search,
			int limit = 0,
			int offset = 0);

	// SELECT * FROM tablename WHERE KEY0 LIKE %value% AND KEY1 LIKE %value% ...
	qval_type GetLike(const std::string& table_name,
			const map_type& search,
			int limit = 0,
			int offset = 0);

    // SELECT arg1, arg2... FROM table_name
    qval_type Select(const std::string& table_name, const std::vector<std::string>& search,
            int limit = 0,
            int offset = 0);
private:
    // Checks weather limit or offset needs to be applied and returns extra values if needed
    void check_limit_params(std::string& query_string, int limit, int offset);
};


#endif /* SQLREADER_H_ */
