#include "sqlmanip.h"

int SQLManip::CreateTable(const std::string& tablename, const map_type& columns)
{
   std::string query_string = "CREATE TABLE " + tablename + " (";
   
   map_type::const_iterator i;
   for(i = columns.begin(); i != --columns.end(); i++)
        query_string += i->first + ' ' + i->second + ", ";

   query_string += i->first + ' ' + i->second + ");";

   return Query(query_string).size() ? QUERY_SUCCESS : QUERY_FAILED;
}

