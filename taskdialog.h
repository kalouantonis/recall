/* 
 * File:   taskdialogview.h
 * Author: slacker
 *
 * Created on May 25, 2013, 4:08 PM
 */

#ifndef TASKDIALOGVIEW_H
#define	TASKDIALOGVIEW_H

#include <wx/msgdlg.h>
#include "views/taskdialogview.h"

typedef struct 
{
    wxString name;
    wxString description;
    wxDateTime date;
} taskData;

class TaskDialog: public TaskDialogView
{
private:
    taskData *data;
public:
    TaskDialog(wxWindow*, wxWindowID, const wxString&);
    
    void OnSave(wxCommandEvent& event);
    void OnExit(wxCommandEvent& event);

    void SetData(const wxString& titleName, const wxString& desc = wxEmptyString,
                const int &hours = wxDateTime::Now().GetHour(), 
                const int &mins = wxDateTime::Now().GetMinute(),
                const wxDateTime& date = wxDateTime());
    
    const taskData* GetData(void) const { return data; }

    virtual ~TaskDialog(void);
};

#endif	/* TASKDIALOGVIEW_H */

