#include <wx/defs.h>

#include "taskdialog.h"

TaskDialog::TaskDialog(wxWindow* parent, wxWindowID id,
        const wxString& title)
    : TaskDialogView(parent, id, title)
{
    // Bind Save button 
    Connect(wxID_SAVE, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(TaskDialog::OnSave));
    

    Connect(wxID_EXIT, wxEVT_COMMAND_BUTTON_CLICKED,
            wxCommandEventHandler(TaskDialog::OnExit));
    
    // Initialize data    
    data = NULL;
}

void TaskDialog::OnSave(wxCommandEvent& WXUNUSED(event))
{
    if(data)
        delete data;

    data = new taskData;
    
    data->name = m_taskName->GetValue();
    data->description = m_description->GetValue();
    data->date = wxDateTime(m_dateCtrl->GetValue().GetDay(),
                         m_dateCtrl->GetValue().GetMonth(),
                         m_dateCtrl->GetValue().GetYear(),
                         m_sHours->GetValue(), m_sMins->GetValue());

    if(!data->name)
    {
        wxMessageBox(wxT("Must enter a Name for the task!"), wxT("No Data!"),
                     wxOK, this);
        return;
    }

    // Check local time values
    else if(data->date.GetValue() <= wxDateTime::Now().GetValue())
    {
    	wxMessageBox(wxT("Can not enter time before or at current time!"), wxT("Incorrect time"),
    			wxOK, this);
    	return;
    }
    
    EndModal(wxID_SAVE);
}

void TaskDialog::OnExit(wxCommandEvent& WXUNUSED(event))
{
    Close(false);
}

void TaskDialog::SetData(const wxString& titleName, const wxString& desc, 
                         const int &hours, const int &mins, 
                         const wxDateTime& date)
{
    m_taskName->SetValue(titleName);
    m_description->SetValue(desc);
    m_sHours->SetValue(hours);
    m_sMins->SetValue(mins);
    m_dateCtrl->SetValue(date);
}

TaskDialog::~TaskDialog(void)
{
    if(data)
        delete data;
    Destroy();
}
