#include "tasklistctrl.h"
    

// Debug
#include <iostream>

//IMPLEMENT_CLASS(TaskListCtrl, wxListCtrl)

// Declare custom event table
BEGIN_EVENT_TABLE(TaskListCtrl, wxListCtrl)
	// Left mouse button pressed
	EVT_LEFT_DOWN(TaskListCtrl::OnMouseEvent)
END_EVENT_TABLE()

TaskListCtrl::TaskListCtrl(wxWindow* parent, wxWindowID id,
                           const wxPoint& pos, const wxSize& size,
                           long style, const wxValidator& validator,
                           const wxString& name)
    : wxListCtrl(parent, id, pos, size, style, validator, name), m_pImageList(16, 16)
{
	SetImageList(&m_pImageList, wxIMAGE_LIST_SMALL);
}

void TaskListCtrl::AddImage(void)
{
#include "icons/checkon.xpm"
#include "icons/checkoff.xpm"

	m_pImageList.Add(wxIcon(checkoff_xpm));
	m_pImageList.Add(wxIcon(checkon_xpm));
}

void TaskListCtrl::OnMouseEvent(wxMouseEvent& event)
{
	if(event.LeftDown())
	{
		int flags;
		long item = HitTest(event.GetPosition(), flags);

		if(item > -1 && (flags & wxLIST_HITTEST_ONITEMICON))
		{
			SetChecked(item, !IsChecked(item));
			std::cout << "Button pressed!" << std::endl;
		}
		else
			event.Skip();
	}
	else
		event.Skip();

}

bool TaskListCtrl::IsChecked(long item) const
{
	wxListItem info;
	info.m_mask = wxLIST_MASK_IMAGE;
	info.m_itemId = item;
	// TODO: Change these with args
	//info.m_col = 3;

	if(GetItem(info))
		return (info.m_image == 1);

	return false;
}

void TaskListCtrl::SetChecked(long item, bool checked)
{
	SetItemImage(item, (checked ? 1 : 0), -1);
	// Red or white colour
	SetItemBackgroundColour(item, checked ? wxColor(172, 172, 172) : wxColor(255, 255, 255));
}

wxString TaskListCtrl::GetCellContentsString(long row_number, int column)
{
    wxListItem row_info;

    // Set the row
    row_info.m_itemId = row_number;
    // Set the column
    row_info.m_col = column;
    // Set the mask
    row_info.m_mask = wxLIST_MASK_TEXT;

    // Get the info and store it in the row_info variable
    GetItem(row_info);

    // Extract the text out of that cell
    return row_info.m_text;
}


