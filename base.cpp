#include <wx/event.h>

// Debugging
#include <iostream>
#include <wx-2.8/wx/defs.h>
#include <wx/iconloc.h>
#include <wx/gdicmn.h>

#include "base.h"

void Base::Init(void)
{
	// TODO: Move to header
	// Include icon bitmap
#include "icons/icon.xpm"
#include "icons/new.xpm"
#include "icons/edit.xpm"
#include "icons/exit.xpm"
#include "icons/remove.xpm"

	// Set app icon
	wxIcon app_icon(icon_xpm);
	SetIcon(app_icon);

	wxIcon new_map(new_xpm);
	wxIcon edit_map(edit_xpm);
	wxIcon exit_map(exit_xpm);
	wxIcon remove_map(remove_xpm);

	// Create sizer
	bSizer = new wxBoxSizer(wxVERTICAL);

	// Create menu bar
	menubar = new wxMenuBar;
	// Create tasks menu item
	tasks = new wxMenu;
	tasks->Append(wxID_NEW, wxT("&New"));
	tasks->Append(wxID_EDIT, wxT("&Edit"));
	tasks->Append(wxID_REMOVE, wxT("&Remove"));

	menubar->Append(tasks, wxT("&Tasks"));
	SetMenuBar(menubar);

	// Create new toolbar	
	wxImage::AddHandler(new wxPNGHandler);

	wxBitmap new_icon(new_map);
	wxBitmap exit_icon(exit_map);
	wxBitmap edit_icon(edit_map);
	wxBitmap remove_icon(remove_map);

	toolbar = CreateToolBar();
	// New Task Button
	toolbar->AddTool(wxID_NEW, new_icon, wxT("New Task"));
	// Edit button
	toolbar->AddTool(wxID_EDIT, edit_icon, wxT("Edit selected task"));
	// Remove button
	toolbar->AddTool(wxID_REMOVE, remove_icon, wxT("Remove selected task"));

	toolbar->AddSeparator();
	// Exit button
	toolbar->AddTool(wxID_EXIT, exit_icon, wxT("Exit Application"));
	toolbar->Realize();

	// Initialize list box
	listbox = new TaskListCtrl(this, wxID_PREVIEW, wxDefaultPosition,
			wxDefaultSize, wxLC_SINGLE_SEL | wxLC_REPORT);

	listbox->InsertColumn(COL_TITLE, wxT("Task Name"), wxLIST_FORMAT_LEFT, 150);
	listbox->InsertColumn(COL_DESC, wxT("Description"), wxLIST_FORMAT_LEFT,
			300);
	listbox->InsertColumn(COL_DATE, wxT("Date"), wxLIST_FORMAT_LEFT, 180);
	listbox->InsertColumn(COL_DONE, wxT("Done?"), wxLIST_FORMAT_LEFT, 50);

	// Set the number of items in the list to 0
	itemCount = 0;

	bSizer->Add(listbox, 1, wxALIGN_CENTER | wxALL | wxEXPAND, 5);
	Layout();

	// Connect events
	Connect(wxID_EXIT, wxEVT_COMMAND_TOOL_CLICKED,
			wxCommandEventHandler(Base::OnQuit));
	Connect(wxID_NEW, wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(Base::OnNew));
	Connect(wxID_EDIT, wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(Base::OnEdit));
	Connect(wxID_REMOVE, wxEVT_COMMAND_MENU_SELECTED,
			wxCommandEventHandler(Base::OnRemove));
}

Base::Base(const wxString& title, wxSize size) :
		wxFrame(NULL, ID_BASE, title, wxDefaultPosition, size)
{
	Init();

	Centre();
}

void Base::AddListItem(const TaskDialog& taskdialog)
{
	const taskData* data = taskdialog.GetData();

	// Add name
	listbox->InsertItem(itemCount, wxString(data->name), COL_TITLE);
	// Add description
	listbox->SetItem(itemCount, COL_DESC, wxString(data->description));
	//TODO: Add time

	// Add date, formatted with custom style
	// %R represents 24 hour HH:MM format
	listbox->SetItem(itemCount, COL_DATE,
			wxString(data->date.Format(wxT("%R"), wxDateTime::Local) +
					wxT(" - ") + data->date.FormatDate()));
	// FIXME: Add done
	//listbox->SetItem(itemCount, COL_DONE, wxT("No"));
	listbox->AddImage();
}

long Base::GetSelectedItem(void)
{
	long selected_item = listbox->GetNextItem(-1, wxLIST_NEXT_ALL,
			wxLIST_STATE_SELECTED);

	// This method will generate a message box automatically
	// if no selected item is found
	if (selected_item == -1)
		wxMessageBox(wxT("Please select an item to perform the action."),
				wxT("No item selected"));

	return selected_item;
}

void Base::OnNew(wxCommandEvent& event)
{
	TaskDialog taskdialog(this, (wxWindowID) wxID_NEW, wxT("New Task"));

	if (taskdialog.ShowModal() == wxID_SAVE)
	{
		// TODO: Call save to SQL here

		AddListItem(taskdialog);
		// Increment count as im adding a new item
		itemCount++;

	}
	// Do nothing
}

void Base::OnEdit(wxCommandEvent& WXUNUSED(event))
{
	long selected_item = GetSelectedItem();

	// No selected items
	if (selected_item == -1)
		return;

	wxString titleName = listbox->GetCellContentsString(selected_item,
			COL_TITLE);
	wxString desc = listbox->GetCellContentsString(selected_item, COL_DESC);

	TaskDialog taskdialog(this, (wxWindowID) wxID_EDIT, wxT("Edit Task"));
	taskdialog.SetData(titleName, desc);

	if (taskdialog.ShowModal() == wxID_SAVE)
	{
		AddListItem(taskdialog);
		// Delete previous item
		listbox->DeleteItem(selected_item);
		// Decrement counter to adjust for deletion
	}
}

void Base::OnRemove(wxCommandEvent& WXUNUSED(event))
{
	long selected_item = GetSelectedItem();

	if (selected_item == -1)
		return;

	if (wxMessageBox(wxT("Are you sure you want to delete this item?"),
			wxT("Delete?"), wxYES_NO | wxCENTRE) == wxYES)
	{
		listbox->DeleteItem(selected_item);
		--itemCount;
	}

	std::cout << itemCount << std::endl;
}

void Base::OnQuit(wxCommandEvent& WXUNUSED(event))
{
	if (wxMessageBox(wxT("Are you sure you want to quit?"), wxT("Quit?"),
			wxYES_NO, this) == wxYES)
		Close();
}

Base::~Base(void)
{
	Destroy();
}
