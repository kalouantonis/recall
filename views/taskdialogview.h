///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 21 2009)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __taskdialog__
#define __taskdialog__

#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/spinctrl.h>
#include <wx/datectrl.h>
#include <wx/dateevt.h>
#include <wx/button.h>
#include <wx/dialog.h>
#include <wx/datetime.h>
#include <wx/choice.h>

///////////////////////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////////////////////////
/// Class TaskDialog
///////////////////////////////////////////////////////////////////////////////
class TaskDialogView : public wxDialog 
{
	protected:
		wxStaticText* m_staticName;
		wxTextCtrl* m_taskName;
		wxStaticText* m_staticDesc;
		wxTextCtrl* m_description;
		wxStaticText* m_staticTime;
		wxSpinCtrl* m_sHours;
		wxSpinCtrl* m_sMins;
		wxStaticText* m_staticDate;
		wxDatePickerCtrl* m_dateCtrl;
		wxButton* m_btnSave;
		wxButton* m_btnCancel;
	
	public:
		
		TaskDialogView( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 400,260 ), long style = wxDEFAULT_DIALOG_STYLE );

        const wxDatePickerCtrl* GetDateCtrl() const 
        {
            return m_dateCtrl;
        }

        const wxSpinCtrl* GetSMins() const 
        {
            return m_sMins;
        }

        const wxSpinCtrl* GetSHours() const 
        {
            return m_sHours;
        }

        const wxTextCtrl* GetDescription() const 
        {
            return m_description;
        }

        const wxTextCtrl* GetTaskName() const 
        {
            return m_taskName;
        }


};

#endif //__noname__
