///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Dec 21 2009)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include <wx-2.8/wx/msgdlg.h>
#include <wx-2.8/wx/defs.h>

#include "taskdialogview.h"

///////////////////////////////////////////////////////////////////////////

TaskDialogView::TaskDialogView( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxDialog( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* taskSizer;
	taskSizer = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* nameSizer;
	nameSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticName = new wxStaticText( this, wxID_ANY, wxT("Task Name:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticName->Wrap( -1 );
	nameSizer->Add( m_staticName, 0, wxALL, 10 );
	
	m_taskName = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 280,-1 ), 0 );
	nameSizer->Add( m_taskName, 0, wxALL, 5 );
	
	taskSizer->Add( nameSizer, 0, wxEXPAND, 20 );
	
	wxBoxSizer* descSizer;
	descSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticDesc = new wxStaticText( this, wxID_ANY, wxT("Description:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticDesc->Wrap( -1 );
	descSizer->Add( m_staticDesc, 0, wxALL, 10 );
	
	m_description = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 280,-1 ), wxTE_MULTILINE|wxVSCROLL );
	descSizer->Add( m_description, 0, wxALL, 5 );
	
	taskSizer->Add( descSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* timeSizer;
	timeSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticTime = new wxStaticText( this, wxID_ANY, wxT("Time (H / M): "), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticTime->Wrap( -1 );
	timeSizer->Add( m_staticTime, 0, wxALL, 10 );
	
	m_sHours = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 40,-1 ), wxSP_ARROW_KEYS, 0, 23,
			wxDateTime::Now().GetHour());
	timeSizer->Add( m_sHours, 0, wxALL, 5 );
	
	m_sMins = new wxSpinCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxSize( 40,-1 ), wxSP_ARROW_KEYS, 0, 59,
			wxDateTime::Now().GetMinute() + 1);
	timeSizer->Add( m_sMins, 0, wxALL, 5 );
	
	taskSizer->Add( timeSizer, 0, wxEXPAND, 5 );

	wxBoxSizer* dateSizer;
	dateSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticDate = new wxStaticText( this, wxID_ANY, wxT("Date:"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticDate->Wrap( -1 );
	dateSizer->Add( m_staticDate, 0, wxALL, 10 );
	
	m_dateCtrl = new wxDatePickerCtrl( this, wxID_ANY, wxDefaultDateTime, wxDefaultPosition, wxSize( 110,-1 ), wxDP_DEFAULT );

    // Use range, set the second param to a false value, so as to
    // set the end range to forever
    m_dateCtrl->SetRange(wxDateTime::Now(), wxDateTime());
    
	dateSizer->Add( m_dateCtrl, 0, wxALL, 5 );
	
	taskSizer->Add( dateSizer, 0, wxEXPAND, 5 );
	
	wxBoxSizer* btnSizer;
	btnSizer = new wxBoxSizer( wxHORIZONTAL );
	
	m_btnSave = new wxButton( this, wxID_SAVE, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
	btnSizer->Add( m_btnSave, 0, wxALL, 10 );
	
	m_btnCancel = new wxButton( this, wxID_EXIT, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	btnSizer->Add( m_btnCancel, 0, wxALL|wxTOP, 10 );
	
	taskSizer->Add( btnSizer, 0, wxALL|wxBOTTOM|wxEXPAND, 5 );
	
	// Set cursor focus to this dialog
	SetFocus();
	SetSizer( taskSizer );
	Layout();

}
