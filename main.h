/* 
 * File:   main.h
 * Author: slacker
 *
 * Created on May 22, 2013, 2:21 PM
 */

#ifndef MAIN_H
#define	MAIN_H

#ifdef WINDOWS_OS

// Write some code to include the MS manifest
#include "wx/msw/wx.rc"

#endif

#include <wx/wx.h>

class Recall: public wxApp
{
	public:
		virtual bool OnInit(void);
};

#endif	/* MAIN_H */

