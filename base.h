/* 
 * File:   base.h
 * Author: slacker
 *
 * Created on May 22, 2013, 2:08 PM
 */

#ifndef BASE_H
#define	BASE_H

#include <wx/wx.h>
#include <wx/listctrl.h>
#include <wx/datetime.h>

#include "taskdialog.h"
#include "tasklistctrl.h"


class Base: public wxFrame
{
public:
	Base(const wxString& title, wxSize size);
	
	void OnNew(wxCommandEvent& event);
    void OnEdit(wxCommandEvent& event);
    void OnRemove(wxCommandEvent& event);
	void OnQuit(wxCommandEvent& event);

    ~Base(void);

protected:
    // Make hours and mins equal local time by default
    void AddListItem (const TaskDialog& taskdialog);
    long GetSelectedItem(void);
private:
	void Init(void);
	
	wxBoxSizer* bSizer;
	TaskListCtrl *listbox;
	wxMenuBar *menubar;
	wxMenu *tasks;
	wxToolBar *toolbar;

	int itemCount;
};

const int ID_BASE = 101;
const int ID_LIST = 102;

const int COL_TITLE = 0;
const int COL_DESC = 1;
const int COL_DATE = 2;
const int COL_DONE = 3;

#endif	/* BASE_H */

