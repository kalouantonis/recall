/* 
 * File:   tasklistctrl.h
 * Author: slacker
 *
 * Created on May 26, 2013, 7:48 PM
 */

#ifndef TASKLISTCTRL_H
#define	TASKLISTCTRL_H

#include <wx/listctrl.h>
#include <wx/string.h>
#include <wx/image.h>
#include <wx/imaglist.h>

class TaskListCtrl: public wxListCtrl
{
    private:
    	wxImageList m_pImageList;

    	void SetChecked(long item, bool checked);

    protected:
    	// Specify the fact that we will be using a custom event table
    	DECLARE_EVENT_TABLE()

	public:
        TaskListCtrl(wxWindow* parent, wxWindowID id,
                const wxPoint& pos = wxDefaultPosition,
                const wxSize& size = wxDefaultSize,
                long style = wxLC_REPORT,
                const wxValidator& validator = wxDefaultValidator,
                const wxString &name = wxListCtrlNameStr);

        wxString GetCellContentsString(long row_number, int column);

        void OnMouseEvent(wxMouseEvent& event);
        void AddImage(void);
        bool IsChecked(long item) const;
};

const int CHECKON = 0;
const int CHECKOFF = 1;

#endif	/* TASKLISTCTRL_H */

